import { Component, OnInit, ViewChild } from '@angular/core';
import { myGlobals } from '../globals';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DatatableService } from '../services/datatable.service';

@Component({
  selector: 'app-areas',
  templateUrl: './areas.component.html',
  styleUrls: ['./areas.component.css']
})
export class AreasComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  items: any;
  NumberOfItems: number;
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  columns: Array<any> = [];

  constructor(private http: HttpClient, private datatableService: DatatableService) {
    myGlobals.sectionTitle = 'Zonas';
    myGlobals.sectionTitleIcon = 'map-signs';
  }

  dtReload() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  dtLoad() {
    this.columns = [];

    this.columns = [
      { "data": "name" },
      { "data": "account" },
      { "data": "dealers" },
      { "data": "dealerships" },
      { "data": "icon" }
    ];

    let ajax = (dataTablesParameters: any, callback) => {
      /*
      this.http
        .get<DataTablesResponse>(
          'http://www.simplycleverdays.com.preproduccion.com/booking/ajax/dt/list.php?filterType=' + this.filterType + '&filterDay=' + this.filterDay +'&filterDealership=' + this.filterDealership + '&_=1596451550588',
          dataTablesParameters
        ).subscribe((resp: any) => {
          this.items = resp.data;
          this.NumberOfItems = resp.data.length;
          $('.dataTables_length>label>select, .dataTables_filter>label>input').addClass('form-control-sm');
          
          callback({
            recordsTotal: resp.data.length,
            recordsFiltered: resp.data.length,
            data: resp.data
          });

          if (this.NumberOfItems > 0) {
            $('.dataTables_empty').css('display', 'none');
          }
        }
      );
      */

      /* TEST */
      let resp = myGlobals.mockAreasList;

      this.items = resp.data;
      this.NumberOfItems = resp.data.length;
      
      $('.dataTables_length>label>select, .dataTables_filter>label>input').addClass('form-control-sm');

      callback({
        recordsTotal: resp.data.length,
        recordsFiltered: resp.data.length,
        data: resp.data
      });
      
      if (this.NumberOfItems > 0) {
        $('.dataTables_empty').css('display', 'none');
      }
      /* FIN TEST */
    };

    let fnDrawCallback;

    this.dtOptions = this.datatableService.dtOptions(this.columns, ajax, fnDrawCallback);
  }

  ngOnInit(): void {
    this.dtLoad();
  }

}
