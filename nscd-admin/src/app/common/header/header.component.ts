import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  username: string = 'Test';
  companyName: string = 'Test';
  logoUrl: string = '/assets/img/logos/proximity.jpg';

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void { }

  isSelected(sectionName: string): boolean {
    let selected = false;

    if (this.router.url.includes(sectionName)) {
      selected = true;
    }

    return selected;
  }

  logout() {
    this.authService.logout();
    this.router.navigate(["/login", {}]);
  }

}
