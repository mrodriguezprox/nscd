import { Component, OnInit } from '@angular/core';
import { Booking } from '../../../models/booking.model';
import { myGlobals } from '../../../globals';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-booking',
  templateUrl: './edit-booking.component.html',
  styleUrls: ['./edit-booking.component.css']
})
export class EditBookingComponent implements OnInit {

  fields = [
    'honorific',
    'name',
    'surname_1',
    'surname_2',
    'documentNumber',
    'address',
    'cp',
    'town',
    'provinceId',
    'phone',
    'email',
    'info',
    'dealershipId',
    'bookingDate',
    'bookingHour',
    'sellerId',
    'dateType',
    'attended',
    'GDPR_COM_COM',
    'GDPR_PERF'
  ];

  data: Booking;

  constructor(private activatedRoute: ActivatedRoute) {
    this.data = {
      id: '',
      honorific: '',
      name: '',
      surname_1: '',
      surname_2: '',
      documentNumber: '',
      address: '',
      cp: '',
      town: '',
      provinceId: '',
      phone: '',
      email: '',
      info: '',
      dealershipId: '',
      bookingDate: '',
      bookingHour: '',
      sellerId: '',
      dateType: '',
      attended: '',
      GDPR_COM_COM: '0',
      GDPR_PERF: '0'
    } as Booking;

    let id = this.activatedRoute.snapshot.params.id;
    myGlobals.sectionTitleIcon = 'user';

    if (id == 'new') {
      myGlobals.sectionTitle = 'Alta cita';
    } else {
      this.data.id = '1'; // AQUÍ SE HARÍA EL GET CITA
      myGlobals.sectionTitle = 'Cliente';
    }
  }

  ngOnInit(): void { }

}
