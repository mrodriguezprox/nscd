import { Component, ViewChild, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { myGlobals } from '../globals';
import { AuthService } from '../services/auth.service';
import { DataTablesResponse } from '../models/DataTablesResponse.model';
import { HttpClient } from '@angular/common/http';
import { DatatableService } from '../services/datatable.service';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  items: any;
  NumberOfItems: number;
  filterType: string = 'ALL';
  filterDay: string = 'ALL';
  filterDealership: string = 'ALL';
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  columns: Array<any> = [];

  constructor(private authService: AuthService, private http: HttpClient, private datatableService: DatatableService) {
    myGlobals.sectionTitle = 'Citas';
    myGlobals.sectionTitleIcon = 'address-book';
  }

  dtReload() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.destroy();
      this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  dtLoad() {
    this.columns = [];

    if (this.authService.isAdmin()) {
      this.columns = [
        { "data": "dealership" },
        { "data": "car" },
        { "data": "dateType" },
        { "data": "booking" },
        { "data": "customer" },						
        { "data": "phone" },
        { "data": "email" },
        { "data": "info" },
        { "data": "attended" },
        { "data": "icon" }
      ];
    } else if (this.authService.isGgzz()) {
      this.columns = [
        { "data": "dealership" },
        { "data": "car" },
        { "data": "dateType" },
        { "data": "booking" },
        { "data": "customer" },						
        { "data": "phone" },
        { "data": "email" },
        { "data": "info" },
        { "data": "attended" }
      ];
    } else if (this.authService.isDealer()) {
      this.columns = [
        { "data": "dealership" },
        { "data": "car" },
        { "data": "dateType" },
        { "data": "booking" },
        { "data": "customer" },						
        { "data": "phone" },
        { "data": "email" },
        { "data": "info" },
        { "data": "attended" },
        { "data": "icon" }
      ];
    } else if (this.authService.isDriver()) {
      this.columns = [
        { "data": "booking" },
        { "data": "driver" },
        { "data": "customer" },
        { "data": "info" },
        { "data": "attended" },
        { "data": "icon" }
      ];
    }

    let ajax = (dataTablesParameters: any, callback) => {
      /*
      this.http
        .get<DataTablesResponse>(
          'http://www.simplycleverdays.com.preproduccion.com/booking/ajax/dt/list.php?filterType=' + this.filterType + '&filterDay=' + this.filterDay +'&filterDealership=' + this.filterDealership + '&_=1596451550588',
          dataTablesParameters
        ).subscribe((resp: any) => {
          this.items = resp.data;
          this.NumberOfItems = resp.data.length;
          $('.dataTables_length>label>select, .dataTables_filter>label>input').addClass('form-control-sm');
          
          callback({
            recordsTotal: resp.data.length,
            recordsFiltered: resp.data.length,
            data: resp.data
          });

          if (this.NumberOfItems > 0) {
            $('.dataTables_empty').css('display', 'none');
          }
        }
      );
      */

      /* TEST */
      let resp = myGlobals.mockBookingList;

      this.items = resp.data;
      this.NumberOfItems = resp.data.length;
      
      $('.dataTables_length>label>select, .dataTables_filter>label>input').addClass('form-control-sm');

      callback({
        recordsTotal: resp.data.length,
        recordsFiltered: resp.data.length,
        data: resp.data
      });
      
      if (this.NumberOfItems > 0) {
        console.log("llego");
        $('.dataTables_empty').css('display', 'none');
      }
      /* FIN TEST */
    };

    let fnDrawCallback;

    this.dtOptions = this.datatableService.dtOptions(this.columns, ajax, fnDrawCallback);
  }

  ngOnInit(): void {
    this.dtLoad();
  }

  isAdmin(): boolean { return this.authService.isAdmin(); }
  isGgzz(): boolean { return this.authService.isGgzz(); }
  isDealer(): boolean { return this.authService.isDealer(); }
  isDriver(): boolean { return this.authService.isDriver(); }

}
