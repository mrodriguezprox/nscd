import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Dashboard';
    myGlobals.sectionTitleIcon = 'dashboard';
  }

  ngOnInit(): void {
  }

}
