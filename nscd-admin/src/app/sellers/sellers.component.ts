import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-sellers',
  templateUrl: './sellers.component.html',
  styleUrls: ['./sellers.component.css']
})
export class SellersComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Asesores comerciales';
    myGlobals.sectionTitleIcon = 'briefcase';
  }

  ngOnInit(): void {
  }

}
