import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Agenda';
    myGlobals.sectionTitleIcon = 'th';
  }

  ngOnInit(): void {
  }

}
