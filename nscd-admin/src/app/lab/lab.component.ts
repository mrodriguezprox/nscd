import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-lab',
  templateUrl: './lab.component.html',
  styleUrls: ['./lab.component.css']
})
export class LabComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'LAB';
    myGlobals.sectionTitleIcon = 'flask';
  }

  ngOnInit(): void {
  }

}
