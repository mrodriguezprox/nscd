import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Mi perfil';
    myGlobals.sectionTitleIcon = 'user';
  }

  ngOnInit(): void {
  }

}
