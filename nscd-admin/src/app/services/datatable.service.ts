import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DatatableService {

  constructor() { }

  dtOptions(columns: any, ajax: any, fnDrawCallback: any) {
    return {
      pagingType: 'full_numbers',
      pageLength: 100,
      serverSide: false, // CAMBIAR CUANDO SE INTEGREN LOS SERVICIOS
      processing: true,
      info: true,
      language: {
        emptyTable: '',
        zeroRecords: 'No hay coincidencias',
        lengthMenu: 'Mostrar _MENU_ elementos',
        search: 'Buscar:',
        info: 'De _START_ a _END_ de _TOTAL_ elementos',
        infoEmpty: 'De 0 a 0 de 0 elementos',
        infoFiltered: '(filtrados de _MAX_ elementos totales)',
        paginate: {
          first: 'Prim.',
          last: 'Últ.',
          next: 'Sig.',
          previous: 'Ant.'
        },
      },
      ajax: ajax,
      fnDrawCallback: fnDrawCallback,
      columns: columns
    }
  }
}
