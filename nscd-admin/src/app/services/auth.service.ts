import { Injectable } from '@angular/core';
import * as moment from "moment";
import { User } from '../models/user.model';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/do';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

    constructor(private http: HttpClient) { }

    login(email: string, password: string) {
        return this.http.post<User>('/api/login', {email, password}).do(res => this.setSession);
    }

    loginTest() {
        const expiresAt = moment().add(60,'second');

        localStorage.setItem('id_token', 'TEST');
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    }
          
    private setSession(authResult: any): any {
        const expiresAt = moment().add(authResult.expiresIn,'second');

        localStorage.setItem('id_token', authResult.idToken);
        localStorage.setItem("expires_at", JSON.stringify(expiresAt.valueOf()) );
    }          

    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
    }

    public isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem("expires_at");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    getRoleId() {
        let roleId = '1'; // TEST
        return roleId;
    }

    isAdmin(): boolean { return true; }
    isGgzz(): boolean { return false; }
    isDealer(): boolean { return false; }
    isDriver(): boolean { return false; }
}