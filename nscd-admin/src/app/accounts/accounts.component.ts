import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Usuarios';
    myGlobals.sectionTitleIcon = 'users';
  }

  ngOnInit(): void {
  }

}
