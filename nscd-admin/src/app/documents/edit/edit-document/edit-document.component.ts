import { Component, OnInit } from '@angular/core';
import { myGlobals } from 'src/app/globals';

@Component({
  selector: 'app-edit-document',
  templateUrl: './edit-document.component.html',
  styleUrls: ['./edit-document.component.css']
})
export class EditDocumentComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Alta documento';
    myGlobals.sectionTitleIcon = 'file';
  }

  ngOnInit(): void {
  }

}
