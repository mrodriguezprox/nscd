import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';
import { Document } from '../models/document.model';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.css']
})
export class DocumentsComponent implements OnInit {
  documentList: Array<Document> = [];
  code: string = "";

  constructor() {
    myGlobals.sectionTitle = 'Documentación';
    myGlobals.sectionTitleIcon = 'file';
  }

  ngOnInit(): void {
  }

}
