import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-reporting',
  templateUrl: './reporting.component.html',
  styleUrls: ['./reporting.component.css']
})
export class ReportingComponent implements OnInit {

  constructor() {
    myGlobals.sectionTitle = 'Reporting';
    myGlobals.sectionTitleIcon = 'chart-bar';
  }

  ngOnInit(): void {
  }

}
