import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route: ActivatedRoute, private router: Router, private authService: AuthService) {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(["/",{}])
    }
  }

  ngOnInit(): void { }

  login() {
    /* TEST */
    this.authService.loginTest();
    console.log("User is logged in");
    this.router.navigateByUrl('/');
    /* FIN TEST */
    
    /*
    this.authService.login('email', 'password')
    .subscribe(
        () => {
            console.log("User is logged in");
            this.router.navigateByUrl('/');
        }
    );
    */
  }
}
