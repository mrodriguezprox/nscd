import { Deserializable } from "./deserializable.model";

export class User implements Deserializable {
  idToken: string;
  expires_at: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}