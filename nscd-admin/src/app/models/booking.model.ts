import { Deserializable } from "./deserializable.model";

export class Booking implements Deserializable {
    id: string;
    honorific: string;
    name: string;
    surname_1: string;
    surname_2: string;
    documentNumber: string;
    address: string;
    cp: string;
    town: string;
    provinceId: string;
    phone: string;
    email: string;
    info: string;
    dealershipId: string;
    bookingDate: string;
    bookingHour: string;
    sellerId: string;
    dateType: string;
    attended: string;
    GDPR_COM_COM: string;
    GDPR_PERF: string;

  deserialize(input: any) {
    Object.assign(this, input);
    return this;
  }
}