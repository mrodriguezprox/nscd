import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { BookingsComponent } from '../bookings/bookings.component';
import { DocumentsComponent } from '../documents/documents.component';
import { ReportingComponent } from '../reporting/reporting.component';
import { AreasComponent } from '../areas/areas.component';
import { DealersComponent } from '../dealers/dealers.component';
import { DealershipsComponent } from '../dealerships/dealerships.component';
import { SellersComponent } from '../sellers/sellers.component';
import { ScheduleComponent } from '../schedule/schedule.component';
import { AccountsComponent } from '../accounts/accounts.component';
import { LabComponent } from '../lab/lab.component';
import { ProfileComponent } from '../profile/profile.component';
import { HeaderComponent } from '../common/header/header.component';
import { FooterComponent } from '../common/footer/footer.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { EditBookingComponent } from '../bookings/edit/edit-booking/edit-booking.component';
import { EditDocumentComponent } from '../documents/edit/edit-document/edit-document.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { SafePipe } from '../pipes/safe.pipe';
import { RolesDirective } from '../directives/roles.directive';


@NgModule({
  declarations: [
    LayoutComponent,
    BookingsComponent,
    DocumentsComponent,
    ReportingComponent,
    AreasComponent,
    DealersComponent,
    DealershipsComponent,
    SellersComponent,
    ScheduleComponent,
    AccountsComponent,
    LabComponent,
    ProfileComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    EditBookingComponent,
    EditDocumentComponent,
    SafePipe,
    RolesDirective
  ],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule,
    BsDropdownModule.forRoot()
  ],
  bootstrap: [LayoutComponent],
  exports: [ SafePipe, RolesDirective ]
})
export class LayoutModule { }
