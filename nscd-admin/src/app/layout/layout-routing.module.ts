import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { AccountsComponent } from '../accounts/accounts.component';
import { AreasComponent } from '../areas/areas.component';
import { BookingsComponent } from '../bookings/bookings.component';
import { DealersComponent } from '../dealers/dealers.component';
import { DealershipsComponent } from '../dealerships/dealerships.component';
import { DocumentsComponent } from '../documents/documents.component';
import { LabComponent } from '../lab/lab.component';
import { ProfileComponent } from '../profile/profile.component';
import { ReportingComponent } from '../reporting/reporting.component';
import { ScheduleComponent } from '../schedule/schedule.component';
import { SellersComponent } from '../sellers/sellers.component';
import { EditBookingComponent } from '../bookings/edit/edit-booking/edit-booking.component';
import { EditDocumentComponent } from '../documents/edit/edit-document/edit-document.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      { path: '', component: DashboardComponent },
      { path: 'accounts', component: AccountsComponent },
      { path: 'areas', component: AreasComponent },
      { path: 'bookings', component: BookingsComponent },
      { path: 'bookings/edit/:id', component: EditBookingComponent},
      { path: 'dealers', component: DealersComponent },
      { path: 'dealerships', component: DealershipsComponent },
      { path: 'documents', component: DocumentsComponent },
      { path: 'documents/edit/:id', component: EditDocumentComponent},
      { path: 'lab', component: LabComponent },
      { path: 'profile', component: ProfileComponent },
      { path: 'reporting', component: ReportingComponent },
      { path: 'schedule', component: ScheduleComponent },
      { path: 'sellers', component: SellersComponent },
    ]
  },
  //{ path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule { }