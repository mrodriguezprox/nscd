import { Component, OnInit } from '@angular/core';
import { myGlobals } from '../globals';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get sectionTitle() {
    return myGlobals.sectionTitle;
  }

  get sectionTitleIcon() {
    return myGlobals.sectionTitleIcon;
  }

}
